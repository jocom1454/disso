/*
Student Number - 100211671
Author - ukk17cbu
This is the hand class for the Blackjack simulation. This is a collection of cards.
This is a serialised class.
 */
package Cards;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.zip.CheckedOutputStream;

import Cards.Card.CompareRank;
import Cards.Card.Rank;
import Cards.Card.Suit;
import Cards.Deck;

public class Hand implements Serializable, Iterable<Card> {

    //This is a declaration of all the attributes
    private static final long serialVersionUID = 300L;
    //This is a list of cards that will act as the hand
    public List<Card> hand1;
    ArrayList<Integer> handValues = new ArrayList<>();

    //This is a constructor that creates an empty hand
    public Hand() {
        this.hand1 = new ArrayList<>();
    }

    //This will add an array list of cards into the current hand
    public Hand(Card[] hand) {
        this();
        this.hand1.addAll(Arrays.asList(hand));
        handValues();
    }

    //This will add all the cards from one hand to another hand
    public Hand(Hand hand) {
        this();
        this.hand1.addAll(hand.hand1);
        handValues();
    }

    //This is an add method that will add a single card to the hand
    public void add(Card card) {
        hand1.add(card);
        handValues();
    }

    //This is an add method that will add a collection of cards to the hand
    public void add(Collection<Card> card) {
        this.hand1.addAll(card);
        handValues();
    }

    //Thi is an add method that will add all the cards from one hand to another hand
    public void add(Hand hand) {
        this.hand1.addAll(hand1);
        handValues();
    }

    //This is a remove method that will remove a single card
    public boolean remove(Card card) {
        //This is creating a boolean result that will tell you if removing the card has been sucessful
        boolean contain = hand1.contains(card);
        if (contain) {
            hand1.remove(card);
            handValues();
        }
        return contain;
    }

    //This is a remove method that will remove all the cards from any given hand
    public boolean remove(Hand hand) {

        boolean result = true;
        for (Card c : hand.hand1) {
            if (!hand.remove(c)) {
                result = false;
                handValues();
            }
        }
        return result;
    }

    //This is a remove method that will remove a card at a particular position
    public Card remove(int i) {
        //This assigns the card that will be removed to a card called c
        Card c = hand1.remove(i);
        //This will then return the card c and run the method handValues()
        handValues();
        return c;
    }

    //This is a method that will determine the value of the current hand
    public ArrayList handValues() {
        int aceValue = 0;
        int maxValue = 0;

        //This is a loop that will iterate through the hand
        for (Card hand12 : hand1) {
            //The max value will calculate the value of the hand if the aces are 11
            maxValue += hand12.getRank().value;
            //This will also count the amount of aces in the hand and increment the aceValue variable
            if (hand12.getRank() == Card.Rank.ACE) {
                aceValue++;
            }
        }
        //This will then clear the handValues array
        handValues.clear();
        //This will also increment the size of array based on the amount of aces
        for (int i = 0; i <= aceValue; i++) {
            handValues.add(0);
        }

        //This sets the first value to the maxValue
        handValues.set(aceValue, maxValue);
        //This will then minus 10 from the maxValue until the array is full
        for (int i = aceValue - 1; i >= 0; i--) {
            handValues.set(i, handValues.get(i + 1) - 10);
        }

        if (handValues.indexOf(1) < 21) {
            int duplicate = handValues.get(0);
            ;
            handValues.add(duplicate);
        }

        return handValues;
    }

    //This is an iterator method that will allow the hand to be traversed in the order they have been added
    @Override
    public Iterator<Card> iterator() {
        return hand1.iterator();
    }

    //This is a toString that will call the toString for each card from the Card class
    public String toString() {
        //This is a new StringBuilder object that will allow us to create a toString for a hand
        StringBuilder str = new StringBuilder();
        //This will iterate through the hand
        for (Card c : this.hand1) {
            //This will call the toString form Card that will fashion how the hand is to be printed
            str.append(c.toString());
            //This is just adding a new line after each iteration
            str.append("\n");
        }
        //This will return the actual toString object created
        return str.toString();
    }

    //This is a sort method that will sort the cards into ascending order
    public void sort() {
        Collections.sort(hand1);
    }

    //This method will compare the cards by rank
    public void sortByRank() {
        //This will use the Collections.sort method but will also call the CompareRank() method from the Card class
        Collections.sort(hand1, new CompareRank());
    }

    //This is a method that will count the number of a certain suit in each hand
    public int countSuit(Card.Suit suit) {
        //This is a variable that will be incremented
        int difference = 0;
        //This will iterate through the hand
        for (int i = 0; i < hand1.size(); i++) {
            //This is an if loop that checks that the suit entered is the same as the suit on the card
            if (hand1.get(i).getSuit() == suit) {
                //If the suit is the same, it will increment the difference variable
                difference++;
            }
        }
        //This then returns the difference variable
        return difference;
    }

    //This is a method that will count the number of a certain rank in each hand
    public int countRank(Card.Rank rank) {
        //This is a variable that will be incremented
        int difference = 0;
        //This will iterate through the hand
        for (int i = 0; i < hand1.size(); i++) {
            //This is an if loop that checks that the rank entered is the same as the rank on the card
            if (hand1.get(i).getRank() == rank) {
                //If the rank is the same, the difference variable will be incremented
                difference++;
            }
        }
        //It will then return the difference variable
        return difference;
    }

    //This is a boolean method that will check if the current hand has a selected suit
    public boolean hasSuit(Suit suit) {
        //This will iterate through the hand
        for (Card card : hand1) {
            //If the selected suit is in the hand then it will
            if (card.getSuit() == suit) {
                //return true
                return true;
            }
        }
        //Otherwise, it will return false
        return false;
    }

    //This method will check if the player has a double that is the correct situation as Basic Strategy dictates
    public boolean checkPlayerDouble() {
        boolean value = false;
        Card firstCard = hand1.get(0);
        int split = countRank(firstCard.rank);
        if (split == 2) {
            Rank check = firstCard.rank;
            if (check == Rank.ACE || check == Rank.EIGHT || check == Rank.SEVEN || check == Rank.SIX
                    || check == Rank.FOUR || check == Rank.THREE || check == Rank.TWO) {
                value = true;
            } else {
                value = false;
            }
        }
        return value;
    }

    //This is a method that will check if both cards in the hand are the same
    public int doubleValue(Hand hand) {
        int cardRank = 0;
        boolean value = hand.checkPlayerDouble();
        Card firstCard = hand.hand1.get(0);
        if (value == true) {
            if (firstCard.rank == Rank.ACE || firstCard.rank == Rank.EIGHT) {
                cardRank = 11;
            } else if (firstCard.rank == Rank.NINE) {
                cardRank = 9;
            } else if (firstCard.rank == Rank.SEVEN) {
                cardRank = 7;
            } else if (firstCard.rank == Rank.SIX) {
                cardRank = 6;
            } else if (firstCard.rank == Rank.FOUR) {
                cardRank = 4;
            } else if (firstCard.rank == Rank.THREE) {
                cardRank = 3;
            } else if (firstCard.rank == Rank.TWO) {
                cardRank = 2;
            }
        } else {
            cardRank = 0;
            return cardRank;
        }
        return cardRank;
    }

    //This method runs through the players hand and finds the hand count based on the Hi-Lo Count
    public int hiLoP(Hand h) {
        int count = 0;
        for (int i = 0; i < h.hand1.size(); i++) {
            if (h.hand1.get(i).rank.ordinal() <= Rank.SIX.ordinal()) {
                count++;
            } else if (h.hand1.get(i).rank.ordinal() >= Rank.TEN.ordinal()) {
                count--;
            }
        }
        return count;
    }

    //This method runs through the dealers hand and finds the hand count based on the Hi-Lo Count
    public int hiLoD(Hand h) {
        int count = 0;
        if (h.hand1.get(0).rank.ordinal() <= Rank.SIX.ordinal()) {
            count++;
        } else if (h.hand1.get(0).rank.ordinal() >= Rank.TEN.ordinal()) {
            count--;
        }

        return count;
    }

    //This method runs through the players hand and finds the hand count based on the Wong's Halves method
    public int wongP(Hand h) {
        int count = 0;
        for (int i = 0; i < h.hand1.size(); i++) {
            if (h.hand1.get(i).rank.ordinal() == Rank.TWO.ordinal() || h.hand1.get(i).rank.ordinal() == Rank.SEVEN.ordinal()) {
                count += 0.5;
            } else if (h.hand1.get(i).rank.ordinal() == Rank.FIVE.ordinal()) {
                count += 1.5;
            } else if (h.hand1.get(i).rank.ordinal() == Rank.THREE.ordinal()
                    || h.hand1.get(i).rank.ordinal() == Rank.FOUR.ordinal()
                    || h.hand1.get(i).rank.ordinal() == Rank.SIX.ordinal()) {
                count++;
            } else if (h.hand1.get(i).rank.ordinal() == Rank.NINE.ordinal()) {
                count -= 0.5;
            } else if (h.hand1.get(i).rank.ordinal() == Rank.TEN.ordinal()
                    || h.hand1.get(i).rank.ordinal() == Rank.ACE.ordinal()) {
                count--;
            }
        }
        return count;
    }

    //This method runs through the dealers hand and finds the hand count based on the Wong's Halves method
    public int wongD(Hand h) {
        int count = 0;
        if (h.hand1.get(0).rank.ordinal() == Rank.TWO.ordinal() || h.hand1.get(0).rank.ordinal() == Rank.SEVEN.ordinal()) {
            count += 0.5;
        } else if (h.hand1.get(0).rank.ordinal() == Rank.FIVE.ordinal()) {
            count += 1.5;
        } else if (h.hand1.get(0).rank.ordinal() == Rank.THREE.ordinal()
                || h.hand1.get(0).rank.ordinal() == Rank.FOUR.ordinal()
                || h.hand1.get(0).rank.ordinal() == Rank.SIX.ordinal()) {
            count++;
        } else if (h.hand1.get(0).rank.ordinal() == Rank.NINE.ordinal()) {
            count -= 0.5;
        } else if (h.hand1.get(0).rank.ordinal() == Rank.TEN.ordinal()
                || h.hand1.get(0).rank.ordinal() == Rank.ACE.ordinal()) {
            count--;
        }
        return count;
    }

    //This method runs through the players hand and finds the hand count based on the KISS 3 method
    public int kissCountP(Hand h) {
        int count = 0;
        for (int i = 0; i < h.hand1.size(); i++) {
            if (h.hand1.get(i).rank.ordinal() == Rank.TWO.ordinal()) {
                count += 0.5;
            } else if (h.hand1.get(i).rank.ordinal() >= Rank.THREE.ordinal()
                    && h.hand1.get(i).rank.ordinal() <= Rank.SEVEN.ordinal()) {
                count++;
            } else if (h.hand1.get(i).rank.ordinal() == Rank.TEN.ordinal()
                    || h.hand1.get(i).rank.ordinal() == Rank.ACE.ordinal()) {
                count--;
            }
        }
        return count;
    }

    //This method runs through the dealers hand and finds the hand count based on the KISS 3 method
    public int kissCountD(Hand h) {
        int count = 0;
        if (h.hand1.get(0).rank.ordinal() == Rank.TWO.ordinal()) {
            count += 0.5;
        } else if (h.hand1.get(0).rank.ordinal() >= Rank.THREE.ordinal()
                && h.hand1.get(0).rank.ordinal() <= Rank.SEVEN.ordinal()) {
            count++;
        } else if (h.hand1.get(0).rank.ordinal() == Rank.TEN.ordinal()
                || h.hand1.get(0).rank.ordinal() == Rank.ACE.ordinal()) {
            count--;
        }
        return count;
    }

    //This method runs through the players hand and finds the hand count based on the KO Count
    public int koCountP(Hand h) {
        int count = 0;
        for (int i = 0; i < hand1.size(); i++) {
            if (hand1.get(i).rank.ordinal() <= Rank.SEVEN.ordinal()) {
                count++;
            } else if (hand1.get(i).rank.ordinal() >= Rank.TEN.ordinal()) {
                count--;
            }
        }
        return count;
    }

    //This method runs through the dealers hand and finds the hand count based on the KO Count
    public int koCountD(Hand h) {
        int count = 0;
        if (hand1.get(0).rank.ordinal() <= Rank.SEVEN.ordinal()) {
            count++;
        } else if (hand1.get(0).rank.ordinal() >= Rank.TEN.ordinal()) {
            count--;
        }
        return count;
    }

    //This method runs through the players hand and finds the hand count based on the Zen Count
    public int zenCountP(Hand h) {
        int count = 0;
        for (int i = 0; i < hand1.size(); i++) {
            if (hand1.get(i).rank.ordinal() <= Rank.THREE.ordinal()
                    || hand1.get(i).rank.ordinal() <= Rank.SEVEN.ordinal()) {
                count++;
            } else if (hand1.get(i).rank.ordinal() >= Rank.FOUR.ordinal()
                    && hand1.get(i).rank.ordinal() <= Rank.SIX.ordinal()) {
                count += 2;
            } else if (hand1.get(i).rank.ordinal() == Rank.TEN.ordinal()) {
                count -= 2;
            } else if (hand1.get(i).rank.ordinal() == Rank.ACE.ordinal()) {
                count--;
            }
        }
        return count;
    }

    //This method runs through the dealers hand and finds the hand count based on the Zen Count
    public int zenCountD(Hand h) {
        int count = 0;
        if (hand1.get(0).rank.ordinal() <= Rank.THREE.ordinal()
                || hand1.get(0).rank.ordinal() <= Rank.SEVEN.ordinal()) {
            count++;
        } else if (hand1.get(0).rank.ordinal() >= Rank.FOUR.ordinal()
                && hand1.get(0).rank.ordinal() <= Rank.SIX.ordinal()) {
            count += 2;
        } else if (hand1.get(0).rank.ordinal() == Rank.TEN.ordinal()) {
            count -= 2;
        } else if (hand1.get(0).rank.ordinal() == Rank.ACE.ordinal()) {
            count--;
        }
        return count;
    }

    //This method runs through the players hand and finds the hand count based on the Omega Count
    public int omegaCountP(Hand h) {
        int count = 0;
        for (int i = 0; i < hand1.size(); i++) {
            if (hand1.get(i).rank.ordinal() <= Rank.THREE.ordinal()
                    || hand1.get(i).rank.ordinal() == Rank.SEVEN.ordinal()) {
                count++;
            } else if (hand1.get(i).rank.ordinal() >= Rank.FOUR.ordinal()
                    && hand1.get(i).rank.ordinal() <= Rank.SIX.ordinal()) {
                count += 2;
            } else if (hand1.get(i).rank.ordinal() == Rank.NINE.ordinal()) {
                count++;
            } else if (hand1.get(i).rank.ordinal() == Rank.TEN.ordinal()) {
                count -= 2;
            }
        }
        return count;
    }

    //This method runs through the dealers hand and finds the hand count based on the Zen Count
    public int omegaCountD(Hand h) {
        int count = 0;
        if (hand1.get(0).rank.ordinal() <= Rank.THREE.ordinal()
                || hand1.get(0).rank.ordinal() == Rank.SEVEN.ordinal()) {
            count++;
        } else if (hand1.get(0).rank.ordinal() >= Rank.FOUR.ordinal()
                && hand1.get(0).rank.ordinal() <= Rank.SIX.ordinal()) {
            count += 2;
        } else if (hand1.get(0).rank.ordinal() == Rank.NINE.ordinal()) {
            count++;
        } else if (hand1.get(0).rank.ordinal() == Rank.TEN.ordinal()) {
            count -= 2;
        }
        return count;
    }

    public static void main(String[] args) {
        Deck d = new Deck();
//        for(int i = 0; i <d.size(); i++){
//            System.out.println(d.getDeck().get(i));
//        }

        Hand testDeal = new Hand();
        testDeal.add(d.deal());


        Card[] card1 = {new Card(Rank.FOUR, Suit.HEARTS),
                new Card(Rank.FOUR, Suit.DIAMONDS)};


        Card jigga = new Card(Rank.SIX, Suit.DIAMONDS);
        Card jigga1 = new Card(Rank.SIX, Suit.HEARTS);
        Hand newDeck = new Hand(card1);
        Hand secondDeck = new Hand(newDeck);
//        newDeck.add(jigga);
//        newDeck.add(jigga1);
        System.out.println(newDeck);
        System.out.println(newDeck.hiLoP(newDeck));
        System.out.println(newDeck.countRank(Rank.FOUR));
        System.out.println(newDeck.doubleValue(newDeck));
        newDeck.sort();
//
//        System.out.println(newDeck.countSuit(Card.Suit.HEARTS));
//        System.out.println(secondDeck.countRank(Rank.KING));
//        System.out.println(secondDeck.hasSuit(Suit.CLUBS));
//
//        System.out.println(secondDeck.toString());
//        System.out.println(newDeck);
////
//        int test = (int) newDeck.handValues.get(1);
//        System.out.println(test);
//        System.out.println(newDeck.handValues.get(1));

    }

}
