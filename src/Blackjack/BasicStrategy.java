/*
Student Number - 100211671
Author - ukk17cbu
This is the Basic Strategy class for the Blackjack simulation. This creates a Basic Strategy object.
This returns the decision made by a player based on the Basic Strategy
 */
package Blackjack;

import Cards.Card;
import Cards.Hand;
import Cards.Card.Rank;

import java.util.ArrayList;

public class BasicStrategy implements Strategy {
    @Override
    //This method works with the Basic Strategy table. The result is either Stand, Hit or Double
    //Decision - 1 is Stand, 2 is Hit, 3 is Double
    public ArrayList option(Hand h, Round r) {
        ArrayList choice = new ArrayList<Integer>(2);
        //Takes in the dealers upCard
        int upCard = r.upCard;
        //Sets the automatic decsion to stand
        int decision = 1;
        //Gets the current players hand amount
        int handAmount = (int) h.handValues().get(0);
        //If the players hand has an ace in it
        if (h.hand1.get(0).rank == Card.Rank.ACE || h.hand1.get(1).rank == Card.Rank.ACE) {
            //If the upCard is 1
            if (upCard == 1) {
                //If the players hand is bigger than 21 they must stand
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    //If the players hand is bigger or equal to 17 they can double
                    if (handAmount >= 17) {
                        decision = 3;
                        //If their hand amount is less than 10 they will hit
                    } else if (handAmount > 10) {
                        decision = 2;
                        //Any other possible occasion the player will stand
                    } else {
                        decision = 1;
                    }
                }
            }
            if (upCard == 2) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 13) {
                            decision = 1;
                        } else if (handAmount == 12 || handAmount <= 9 && handAmount >= 5) {
                            decision = 2;
                        } else if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        }
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 3) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 13) {
                            decision = 1;
                        } else if (handAmount == 12 || handAmount >= 8) {
                            decision = 2;
                        } else if (handAmount <= 11 && handAmount >= 9) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        }
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 4) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount >= 9 && handAmount <= 11) {
                            decision = 3;
                        } else if (handAmount <= 8) {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        }
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 5) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount >= 9 && handAmount <= 11) {
                            decision = 3;
                        } else if (handAmount <= 8) {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        }
                    }
                }
            }
            if (upCard == 6) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount >= 9 && handAmount <= 11) {
                            decision = 3;
                        } else if (handAmount <= 8) {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        }
                    }
                }
            }
            if (upCard == 7) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount >= 12 && handAmount <= 16 || handAmount <= 9) {
                            decision = 2;
                        }
                        if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 8) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount >= 12 && handAmount <= 16 || handAmount <= 9) {
                            decision = 2;
                        }
                        if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 9) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount >= 12 && handAmount <= 16 || handAmount <= 9) {
                            decision = 2;
                        }
                        if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 10) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount <= 16 && handAmount >= 12 || handAmount <= 10) {
                            decision = 2;
                        }
                        if (handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 11) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            //If the players hand does not have an ace
        } else if (h.hand1.get(0).rank != Card.Rank.ACE || h.hand1.get(1).rank != Card.Rank.ACE) {
            if (upCard == 1) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (handAmount >= 12) {
                        decision = 1;
                    } else {
                        decision = 2;
                    }
                }
            }
            if (upCard == 2) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    //Checks if the player has two of the same cards that does not need to be split, they can double
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else if (h.hand1.get(0).rank == Card.Rank.FOUR || h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 2;
                        } else {
                            if (handAmount >= 13) {
                                decision = 1;
                            } else if (handAmount == 11 || handAmount == 10) {
                                decision = 3;
                            } else {
                                decision = 2;
                            }
                        }
                    }
                }
            }
            if (upCard == 3) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Rank.FIVE) {
                            decision = 3;
                        } else if (h.hand1.get(0).rank == Rank.FOUR || h.hand1.get(0).rank == Rank.TEN) {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 13) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 4) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else if (h.hand1.get(0).rank == Card.Rank.FOUR || h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 5) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE)
                            decision = 3;
                    } else {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 6) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE)
                            decision = 3;
                    } else {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 7) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.SIX || h.hand1.get(0).rank == Card.Rank.FOUR) {
                            decision = 2;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 8) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 9) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 10) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 11) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
        }
        choice.add(0, decision);

        if (decision == 1) {
            choice.add(1, 1);
        } else if (decision == 2) {
            choice.add(1, 1);
        } else if (decision == 3) {
            choice.add(1, 2);
        }
        return choice;
    }

    //This method was originally going to keep track of the count and then be called in the above method to change
    //the bet amount. Game was built in such a way that this didn't work, it was done in BasicPlayer
    @Override
    public int count(BasicPlayer[] players) {
        return 0;
    }
}
