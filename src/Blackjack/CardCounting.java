/*
Student Number - 100211671
Author - ukk17cbu
This is the Card Counting class for the Blackjack simulation.
The concept was to create Card Counting objects but due to time this was not possible,
this class was only used to create the methods and then they were moved to BasicPlayer.
THIS CLASS IS NOT USED.
 */
package Blackjack;

public class CardCounting {

    public int setHiLoCount(BasicPlayer[] players) {
        int hiLoCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.hiLoP(players[1].playersHand);
        int player2Count = players[2].playersHand.hiLoP(players[2].playersHand);
        int player3Count = players[3].playersHand.hiLoP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.hiLoD(players[0].playersHand);
        hiLoCount += playerValues + dealer;
        return hiLoCount;
    }

    public int setWongCount(BasicPlayer[] players){
        int wongCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.wongP(players[1].playersHand);
        int player2Count = players[2].playersHand.wongD(players[2].playersHand);
        int player3Count = players[3].playersHand.wongP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.wongD(players[0].playersHand);
        wongCount += playerValues + dealer;
        return wongCount;
    }

    public int setKissCount(BasicPlayer[] players){
        int kissCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.kissCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.kissCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.kissCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.kissCountD(players[0].playersHand);
        kissCount += playerValues + dealer;
        return kissCount;
    }

    public int setKOCount(BasicPlayer[] players){
        int koCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.koCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.koCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.koCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.koCountD(players[0].playersHand);
        koCount += playerValues + dealer;
        return koCount;
    }

    public int zenCount(BasicPlayer[] players){
        int zenCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.zenCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.zenCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.zenCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.zenCountD(players[0].playersHand);
        zenCount += playerValues + dealer;
        return zenCount;
    }

    public int omegaCount(BasicPlayer[] players){
        int omegaCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.omegaCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.omegaCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.omegaCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.omegaCountD(players[0].playersHand);
        omegaCount += playerValues + dealer;
        return omegaCount;
    }
}
