/*
Student Number - 100211671
Author - ukk17cbu
This is the Basic Player class for the Blackjack simulation. This creates a Basic Player.
A player has a hand, split hand, strategy and running count.
 */
package Blackjack;

import Cards.Card;
import Cards.Hand;

import java.util.ArrayList;

public class BasicPlayer implements Player {

    //These are the attributes that make up a player
    public Hand playersHand;
    public Hand splitHand;
    Strategy strategy;
    public Player currentPlayer;
    public int playerID;
    public int runningCount;

    public BasicPlayer(int playerID, Hand playersHand, Hand splitHand, Strategy strategy) {
        this.playersHand = playersHand;
        this.splitHand = splitHand;
        this.strategy = strategy;
        this.playerID = playerID;
    }

    //This sets the player ID
    public int setID(Round r) {
        for (int i = 0; i < r.players.length; i++) {
            playerID = i;
        }
        return playerID;
    }

    //This uses both the Hi-Lo methods in Hand to find a count for the deck
    public int setHiLoCount(BasicPlayer[] players) {
        int hiLoCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.hiLoP(players[1].playersHand);
        int player2Count = players[2].playersHand.hiLoP(players[2].playersHand);
        int player3Count = players[3].playersHand.hiLoP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.hiLoD(players[0].playersHand);
        hiLoCount += playerValues + dealer;
        return hiLoCount;
    }

    //This uses both the Wong Halves methods in Hand to find a count for the deck
    public int setWongCount(BasicPlayer[] players){
        int wongCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.wongP(players[1].playersHand);
        int player2Count = players[2].playersHand.wongD(players[2].playersHand);
        int player3Count = players[3].playersHand.wongP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.wongD(players[0].playersHand);
        wongCount += playerValues + dealer;
        return wongCount;
    }

    //This uses both the KISS methods in Hand to find a count for the deck
    public int setKissCount(BasicPlayer[] players){
        int kissCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.kissCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.kissCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.kissCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.kissCountD(players[0].playersHand);
        kissCount += playerValues + dealer;
        return kissCount;
    }

    //This uses both the KO Count methods in Hand to find a count for the deck
    public int setKOCount(BasicPlayer[] players){
        int koCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.koCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.koCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.koCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.koCountD(players[0].playersHand);
        koCount += playerValues + dealer;
        return koCount;
    }

    //This uses both the Zen Count methods in Hand to find a count for the deck
    public int setZenCount(BasicPlayer[] players){
        int zenCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.zenCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.zenCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.zenCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.zenCountD(players[0].playersHand);
        zenCount += playerValues + dealer;
        return zenCount;
    }

    //This uses both the Omega Count methods in Hand to find a count for the deck
    public int setOmegaCount(BasicPlayer[] players){
        int omegaCount = 0;
        int playerValues = 0;
        int dealer = 0;

        int player1Count = players[1].playersHand.omegaCountP(players[1].playersHand);
        int player2Count = players[2].playersHand.omegaCountP(players[2].playersHand);
        int player3Count = players[3].playersHand.omegaCountP(players[3].playersHand);

        playerValues = player1Count + player2Count + player3Count;
        dealer = players[0].playersHand.omegaCountD(players[0].playersHand);
        omegaCount += playerValues + dealer;
        return omegaCount;
    }

    //This method adds a card to the players hand when it is dealt
    @Override
    public void dealCard(Card c) {
        playersHand.hand1.add(c);
    }

    //This method sets the strategy used by a player
    @Override
    public void setStrategy(Strategy s) {
        strategy = s;
    }

    //This method calls the Basic Strategy method and returns the choice made by the player
    @Override
    public ArrayList choice(Round r) {
        System.out.println("This player is - " + playerID);
        System.out.println(playersHand.handValues().get(0));
        int choice = (int) strategy.option(playersHand, r).get(0);
        String decision = "blank";
        if (choice == 1) {
            decision = "Stand";
        } else if (choice == 2) {
            decision = "Hit";
        } else if (choice == 3) {
            decision = "DOUBLE";
        }
        if (decision == "blank") {
            System.out.println(choice);
            System.out.println("found you");
        }
        System.out.println("This player is going to - " + decision + "\n");
        return strategy.option(playersHand, r);
    }

    //This was a test method to alter the betting amount, was changed later on
    public int incrementBS(Round r) {
        int increment = strategy.count(r.players);
        if (increment == 1 || increment == 2) {
            increment = 1;
        } else {
            increment = 2;
        }
        return increment;
    }

    //Allows the player to see the round
    @Override
    public void viewRound(Round r) {
        for (Card cards : r.floor) {
            System.out.println(cards);
        }
    }

    //Get method for playerID
    @Override
    public int getID() {
        return playerID;
    }

}
