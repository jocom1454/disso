/*
Student Number - 100211671
Author - ukk17cbu
This is the Dealer Strategy class for the Blackjack simulation.
The dealer will stand at 17 and hit otherwise.
 */
package Blackjack;

import Cards.Hand;

import java.util.ArrayList;

public class DealerStrategy implements Strategy {
    @Override
    public ArrayList option(Hand h, Round r) {
        ArrayList choice = new ArrayList<Integer>(2);
        int decision = 0;
        int handAmount = (int) h.handValues().get(0);
        if (handAmount > 21) {
            decision = 1;
            choice.add(0, decision);
        } else {
            if (handAmount >= 17) {
                decision = 1;
                choice.add(0, decision);
            } else {
                decision = 2;
                choice.add(0, decision);

            }
        }
        return choice;
    }

    @Override
    public int count(BasicPlayer[] players) {
        return 0;
    }
}
