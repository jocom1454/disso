/*
Student Number - 100211671
Author - ukk17cbu
This is the Strategy Interface class for the Blackjack simulation. This is a collection of cards.
 */
package Blackjack;

import Cards.Hand;

import java.util.ArrayList;

/**
 *
 * Programming 2 coursework file
 * @author ajb
 */
public interface Strategy {
    /**
     * Choose a card from hand h to play in trick t
     * @param h

     * @return
     */
    ArrayList option(Hand h, Round r);
    /**
     * Update internal memory to include completed trick c
     */
    int count(BasicPlayer[] players);
    /**
     * Update internal memory to include completed trick c
     */

}
