/*
Student Number - 100211671
Author - ukk17cbu
This is the HiLo Strategy class for the Blackjack simulation.
The original idea was to make all card counting methods different strategies,
as they all use basic strategy. There was too little time to change the structure to make this work
so all the card counting methods were stored in BasicPlayer.
THIS CLASS IS NOT USED.
 */
package Blackjack;

import Cards.Card;
import Cards.Hand;

import java.util.ArrayList;

public class HiLoStrategy implements Strategy {
    @Override
    //Decision - 1 is Stand, 2 is Hit, 3 is Double
    public ArrayList option(Hand h, Round r) {
        ArrayList choice = new ArrayList<Integer>(2);
        int upCard = r.upCard;
        int decision = 1;
        int handAmount = (int) h.handValues().get(0);
        if (h.hand1.get(0).rank == Card.Rank.ACE || h.hand1.get(1).rank == Card.Rank.ACE) {
            if (upCard == 1) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (handAmount >= 17) {
                        decision = 3;
                    } else if (handAmount > 10) {
                        decision = 2;
                    } else {
                        decision = 1;
                    }
                }
            }
            if (upCard == 2) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 13) {
                            decision = 1;
                        } else if (handAmount == 12 || handAmount <= 9 && handAmount >= 5) {
                            decision = 2;
                        } else if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        }
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 3) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 13) {
                            decision = 1;
                        } else if (handAmount == 12 || handAmount >= 8) {
                            decision = 2;
                        } else if (handAmount <= 11 && handAmount >= 9) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        }
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 4) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount >= 9 && handAmount <= 11) {
                            decision = 3;
                        } else if (handAmount <= 8) {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        }
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 5) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount >= 9 && handAmount <= 11) {
                            decision = 3;
                        } else if (handAmount <= 8) {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        }
                    }
                }
            }
            if (upCard == 6) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount >= 9 && handAmount <= 11) {
                            decision = 3;
                        } else if (handAmount <= 8) {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        }
                    }
                }
            }
            if (upCard == 7) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount >= 12 && handAmount <= 16 || handAmount <= 9) {
                            decision = 2;
                        }
                        if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 8) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount >= 12 && handAmount <= 16 || handAmount <= 9) {
                            decision = 2;
                        }
                        if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 9) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount >= 12 && handAmount <= 16 || handAmount <= 9) {
                            decision = 2;
                        }
                        if (handAmount == 10 || handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 10) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        }
                        if (handAmount <= 16 && handAmount >= 12 || handAmount <= 10) {
                            decision = 2;
                        }
                        if (handAmount == 11) {
                            decision = 3;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 11) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (!h.checkPlayerDouble()) {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
        } else if (h.hand1.get(0).rank != Card.Rank.ACE || h.hand1.get(1).rank != Card.Rank.ACE) {
            if (upCard == 1) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (handAmount >= 12) {
                        decision = 1;
                    } else {
                        decision = 2;
                    }
                }
            }
            if (upCard == 2) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else if (h.hand1.get(0).rank == Card.Rank.FOUR || h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 2;
                        } else {
                            if (handAmount >= 13) {
                                decision = 1;
                            } else if (handAmount == 11 || handAmount == 10) {
                                decision = 3;
                            } else {
                                decision = 2;
                            }
                        }
                    }
                }
            }
            if (upCard == 3) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else if (h.hand1.get(0).rank == Card.Rank.FOUR || h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 13) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 4) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else if (h.hand1.get(0).rank == Card.Rank.FOUR || h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 5) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE)
                            decision = 3;
                    } else {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 6) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE)
                            decision = 3;
                    } else {
                        if (handAmount >= 12) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10 || handAmount == 9) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 7) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.SIX || h.hand1.get(0).rank == Card.Rank.FOUR) {
                            decision = 2;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 8) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 9) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN) {
                            decision = 1;
                        } else if (h.hand1.get(0).rank == Card.Rank.FIVE) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11 || handAmount == 10) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 10) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else if (handAmount == 11) {
                            decision = 3;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
            if (upCard == 11) {
                if (handAmount > 21) {
                    decision = 1;
                } else {
                    if (h.checkPlayerDouble()) {
                        if (h.hand1.get(0).rank == Card.Rank.TEN || h.hand1.get(0).rank == Card.Rank.NINE) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    } else {
                        if (handAmount >= 17) {
                            decision = 1;
                        } else {
                            decision = 2;
                        }
                    }
                }
            }
        }
        choice.add(0, decision);
        if (decision == 1) {
            choice.add(1, 1);
        } else if (decision == 2) {
            choice.add(1, 1);
        } else if (decision == 3) {
            choice.add(1, 2);
        }
        return choice;
    }

    @Override
    public int count(BasicPlayer[] players) {
        int count = 0;
        return count;
    }
}
