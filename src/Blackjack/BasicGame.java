/*
Student Number - 100211671
Author - ukk17cbu
This is the BasicGame class for the Blackjack simulation.
This class runs a full game.
 */
package Blackjack;


import Cards.Card;
import Cards.Deck;
import Cards.Hand;

import java.util.ArrayList;
import java.util.Collections;

public class BasicGame {
    //This sets the number of players, keeps track of how many hands hands have been played and the playerPoints
    static BasicPlayer[] players;
    static final int numberOfPlayers = 4;
    private static int gameCount;
    static int[] playerPoints;
    static ArrayList<Integer> testing = new ArrayList<Integer>();


    public BasicGame(BasicPlayer[] p1) {
        players = p1;
    }

    //This deals 8 cards to the 3 players and dealer.
    public void dealHands(Deck newBDeck) {
        for (int i = 0; i < 8; i++) {
            players[i % numberOfPlayers].dealCard(newBDeck.deal());
        }
    }

    //This sets up the round to be played
    public Round round(BasicPlayer firstPlayer) {
        Round r = new Round(firstPlayer.getID());
        int playerID = firstPlayer.getID();
        for (int i = 0; i < numberOfPlayers; i++) {
            int nextTurn = (playerID + 1) % numberOfPlayers;
        }
        return r;
    }

    //This will loop through a players hand and find if they need to split.
    public boolean checkForSplit() {
        boolean decision = false;
        //This gets the dealers upCard
        Card upCard = players[0].playersHand.hand1.get(0);
        //Loops through the player array
        for (int i = 1; i < players.length; i++) {
            Hand playersHand = players[i].playersHand;
            Card playersFirst = playersHand.hand1.get(0);
            Card.Rank playersRank = playersFirst.rank;
            //If the player has a double this occurs
            if (playersHand.checkPlayerDouble()) {
                if (upCard.rank == Card.Rank.TWO) {
                    if (playersRank != Card.Rank.FOUR) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.THREE) {
                    if (playersRank != Card.Rank.FOUR) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.FOUR) {
                    if (playersRank != Card.Rank.FOUR) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.FIVE) {
                    decision = true;

                } else if (upCard.rank == Card.Rank.SIX) {
                    decision = true;

                } else if (upCard.rank == Card.Rank.SEVEN) {
                    if (playersRank == Card.Rank.SEVEN) {
                        decision = true;
                    } else if (playersRank == Card.Rank.THREE) {
                        decision = true;
                    } else if (playersRank == Card.Rank.TWO) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.EIGHT) {
                    if (playersRank == Card.Rank.NINE) {
                        decision = true;
                    }
                } else if (upCard.rank == Card.Rank.NINE) {
                    if (playersRank == Card.Rank.NINE) {
                        decision = true;
                    }
                }
            }
        }
        //If decision is true, the player can split, if not the player sticks with their hand
        return decision;
    }

    //This plays a match
    public void playMatch() {
        Deck d = new Deck();
        //Creates a new Blackjack deck
        d.newBDeck();
        //This is printed to ensure that a 6 deck is created
        System.out.println(d.size());
        //Initialises the round
        Round r = round(players[0]);
        //Deals cards to players
        dealHands(d);
        r.upCard = players[0].playersHand.hand1.get(0).rank.getValue();
        //This variable checks if the player has hit on their original hand, as you cannot bet after playing
        int firstTry = 0;
        //This variable checks if the player has hit on their split hand, as you cannot bet after playing
        int secondTry = 0;
        //Keeps track of how many hands are played
        int howLongs = 0;
        //This keeps track of how many hands are split
        int splitValue = 0;
        //While a full round can be played, 20 was the sure amount
        while (d.size() > 20) {
            howLongs++;
            System.out.println("-----------------ROUND START-----------------");
            //Print out the dealer's upCard
            System.out.println(r.upCard);
            System.out.println("PLAYERS HANDS");
            //This changes the shuffle point
//            if(d.size() < 75){
//                Collections.shuffle(d.getbDeck());
//            }
            //Print all players hands
            for (int i = 0; i < players.length; i++) {
                System.out.println("PLAYER " + i + " HAND IS - ");
                System.out.println(players[i].playersHand);
            }
            //Checks if the player wants to split their hand
            System.out.println("CHECKING FOR SPLITS");
            for (int i = 1; i < players.length; i++) {
                if (players[i].playersHand.checkPlayerDouble()) {
                    if (checkForSplit()) {
                        splitValue++;
                        System.out.println("PLAYER " + i + " HAS A DOUBLE" + "\n");
                        System.out.println("THEIR HAND BEFORE SPLIT IS - ");
                        System.out.println(players[i].playersHand);
                        //This moves one card to the players split hand
                        Card cardToMove = players[i].playersHand.hand1.get(1);
                        players[i].splitHand.add(cardToMove);
                        players[i].playersHand.remove(1);
                        //Both the split hand and the original hand are then dealt a card
                        players[i].playersHand.add(d.deal());
                        players[i].splitHand.add(d.deal());
                        System.out.println("THEIR HAND AFTER SPLIT IS - ");
                        System.out.println(players[i].playersHand);
                        System.out.println("THEIR SPLIT HAND IS - ");
                        System.out.println(players[i].splitHand);
                    }
                }
            }
            //This checked if the count was correct
//            System.out.println("METHOD COUNT CHECK");
//            System.out.println(players[1].playersHand.hiLoP(players[1].playersHand));
//            System.out.println("METHOD COUNT CHECK FINISHED " + "\n");

            System.out.println("CHOICES MADE");
            //Ensure all players are standing before continuing
            for (int i = 0; i < players.length; i++) {
                //Use method from BasicPlayer to see if they need to hit or not
                int decision = (int) players[i].choice(r).get(0);
                System.out.println("THE DECISION IS - " + decision);
                //If player wants to hit this happens
                if (decision == 2) {
                    //While loop to continue checking if they want to hit
                    while ((int) players[i].choice(r).get(0) == 2) {
                        firstTry++;
                        //This prints out the choice and their new hand value
                        System.out.println("The choice value is - " + players[i].choice(r) + "\n");
                        System.out.println("PLAYER " + i + " WANTS TO HIT" + "\n");
                        System.out.println("The current hand value is - " + players[i].playersHand.handValues().get(0));
                        //Add another card to players hand
                        players[i].playersHand.add(d.deal());
//                    System.out.println("CHECKING IF ADDED TO HAND");
//                    System.out.println(players[i].playersHand);
                        System.out.println("The new hand value is - " + players[i].playersHand.handValues().get(0));
                    }
                }
                //If player wants to stand
                if (decision == 1) {
                    System.out.println("PLAYER " + i + " WILL STAND" + "\n");

                    //If player wants to double
                } else if (decision == 3) {
                    System.out.println("PLAYER " + i + " WILL DOUBLE" + "\n");
                }

            }
            System.out.println("ALL PLAYERS SATISFIED, LETS CHECK EM!" + "\n");
            //Check all players hands against dealer
            for (int i = 1; i < players.length; i++) {
                System.out.println("PLAYER " + i + " HAND WILL BE CHECKED NOW" + "\n");
                int dealerAmount = (int) players[0].playersHand.handValues().get(0);
                System.out.println("THIS IS THE DEALER AMOUNT - " + dealerAmount);
                System.out.println("THIS IS THE PLAYER AMOUNT - " + players[i].playersHand.handValues().get(0));
                int currentPlayerAmount = (int) players[i].playersHand.handValues().get(0);
                int decision = (int) players[i].choice(r).get(0);
                //This sets the bet amount
                int increment = 0;
                if (secondTry > 1) {
                    increment = 1;
                } else {
                    increment = (int) players[i].choice(r).get(1);
                    //CHANGE CARD COUNTING METHOD HERE
                    if(players[i].setOmegaCount(players) > 3){
                        //If the count is high, the bet will be increased
//                        System.out.println("I MADE IT HERE HA 1");
                        increment = 5;
                    } else if(players[i].setOmegaCount(players) > 1){
//                        System.out.println("I MADE IT HERE HA 2");
                        increment = 2;
                    }
                    else if(players[i].setOmegaCount(players) < 1) {
//                        System.out.println("I MADE IT HERE HA 3");
                        increment = (int) players[i].choice(r).get(1);
                    }
                }
                //Prints the results and changes player points
                if (dealerAmount > 21) {
                    if (decision == 3) {
                        System.out.println("PLAYER DOUBLED AND DEALER BUST");
                        for (int j = 0; j < increment; j++) {
                            playerPoints[i]++;
                            playerPoints[0]--;
                        }
                    } else {
                        System.out.println("DEALER BUST");
                        for (int j = 0; j < increment; j++) {
                            playerPoints[i]++;
                            playerPoints[0]--;
                        }
                    }
                } else if (currentPlayerAmount > 21) {
                    if (decision == 3) {
                        System.out.println("PLAYER " + i + " DOUBLED AND BUST" + "\n");
                        for (int j = 0; j < increment; j++) {
                            playerPoints[i]--;
                            playerPoints[0]++;
                            increment = increment*-1;
                        }
                    } else {
                        System.out.println("PLAYER " + i + " BUST" + "\n");
                        for (int j = 0; j < increment; j++) {
                            playerPoints[i]--;
                            playerPoints[0]++;
                            increment = increment*-1;
                        }
                    }
                } else {
                    if (currentPlayerAmount > dealerAmount) {
                        if (decision == 3) {
                            System.out.println("PLAYER " + i + " DOUBLED AND HAS WON THIS ROUND" + "\n");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]++;
                                playerPoints[0]--;
                            }
                        } else {
                            System.out.println("PLAYER " + i + " HAS WON THIS ROUND" + "\n");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]++;
                                playerPoints[0]--;
                            }
                        }
                    } else if (currentPlayerAmount < dealerAmount) {
                        if (decision == 3) {
                            System.out.println("PLAYER " + i + " DOUBLED AND HAS LOST THIS ROUND" + "\n");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]--;
                                playerPoints[0]++;
                                increment = increment*-1;
                            }
                        } else {
                            System.out.println("PLAYER " + i + " HAS LOST THIS ROUND" + "\n");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]--;
                                playerPoints[0]++;
                                increment = increment*-1;
                            }
                        }
                    } else if (currentPlayerAmount == dealerAmount) {
                        System.out.println("BOTH AMOUNTS THE SAME, FOLD" + "\n");
                    }
                }
                firstTry = 0;
                testing.add(increment);

            }
            //Same process for split hands
            System.out.println("CHOICES MADE FOR SPLIT HANDS");
            //Ensure all players are standing before continuing
            for (int i = 0; i < players.length; i++) {
                int decision = (int) players[i].choice(r).get(0);
                while ((int) players[i].choice(r).get(0) == 2) {
                    secondTry++;
                    System.out.println("PLAYER " + i + " WANTS TO HIT" + "\n");
                    System.out.println("The current hand value is - " + players[i].splitHand.handValues().get(0));
                    players[i].splitHand.add(d.deal());
//                    System.out.println("CHECKING IF ADDED TO HAND");
//                    System.out.println(players[i].playersHand);
                    System.out.println("The new hand value is - " + players[i].splitHand.handValues().get(0));
                }
                if (decision == 1) {
                    System.out.println("PLAYER " + i + " WILL STAND" + "\n");

                } else if (decision == 3) {
                    System.out.println("PLAYER " + i + " WILL DOUBLE" + "\n");

                }

            }
            System.out.println("ALL PLAYERS SATISFIED, LETS CHECK EM!" + "\n");
            for (int i = 1; i < players.length; i++) {
                int decision = (int) players[i].choice(r).get(0);
                int increment = 0;
                //CHANGE CARD COUNTING METHOD HERE

                if (firstTry > 1) {
                    increment = 1;
                } else {
                    increment = (int) players[i].choice(r).get(1);
                    //CHANGE CARD COUNTING METHOD HERE
                    if(players[i].setOmegaCount(players) > 3){
//                        System.out.println("I MADE IT HERE HA 1");
                        increment = 5;
                    } else if(players[i].setOmegaCount(players) > 1){
//                        System.out.println("I MADE IT HERE HA 2");
                        increment = 2;
                    }
                    else if(players[i].setOmegaCount(players) < 1) {
//                        System.out.println("I MADE IT HERE HA 3");
                        increment = (int) players[i].choice(r).get(1);
                    }
                }
                if (!players[i].splitHand.hand1.isEmpty()) {
                    System.out.println("PLAYER " + i + " SPLIT HAND WILL BE CHECKED NOW" + "\n");
                    int dealerAmount = (int) players[0].playersHand.handValues().get(0);
                    System.out.println("THIS IS THE DEALER AMOUNT - " + dealerAmount);
                    System.out.println("THIS IS THE PLAYER AMOUNT - " + players[i].splitHand.handValues().get(0));
                    int currentPlayerAmount = (int) players[i].splitHand.handValues().get(0);
                    if (dealerAmount > 21) {
                        if (decision == 3) {
                            System.out.println("PLAYER DOUBLED AND DEALER BUST");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]++;
                                playerPoints[0]--;
                            }
                        } else {
                            System.out.println("DEALER BUST");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]++;
                                playerPoints[0]--;
                            }
                        }
                    } else if (currentPlayerAmount > 21) {
                        if (decision == 3) {
                            System.out.println("PLAYER " + i + " DOUBLED AND BUST" + "\n");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]--;
                                playerPoints[0]++;
                            }
                        } else {
                            System.out.println("PLAYER " + i + " BUST" + "\n");
                            for (int j = 0; j < increment; j++) {
                                playerPoints[i]--;
                                playerPoints[0]++;
                            }
                        }
                    } else {
                        if (currentPlayerAmount > dealerAmount) {
                            if (decision == 3) {
                                System.out.println("PLAYER " + i + " DOUBLED AND HAS WON THIS ROUND" + "\n");
                                for (int j = 0; j < increment; j++) {
                                    playerPoints[i]++;
                                    playerPoints[0]--;
                                }
                            } else {
                                System.out.println("PLAYER " + i + " HAS WON THIS ROUND" + "\n");
                                for (int j = 0; j < increment; j++) {
                                    playerPoints[i]++;
                                    playerPoints[0]--;
                                }
                            }
                        } else if (currentPlayerAmount < dealerAmount) {
                            if (decision == 3) {
                                System.out.println("PLAYER " + i + " DOUBLED AND HAS LOST THIS ROUND" + "\n");
                                for (int j = 0; j < increment; j++) {
                                    playerPoints[i]--;
                                    playerPoints[0]++;
                                }
                            } else {
                                System.out.println("PLAYER " + i + " HAS LOST THIS ROUND" + "\n");
                                for (int j = 0; j < increment; j++) {
                                    playerPoints[i]--;
                                    playerPoints[0]++;
                                }
                            }
                        } else if (currentPlayerAmount == dealerAmount) {
                            System.out.println("BOTH AMOUNTS THE SAME, FOLD" + "\n");
                        }
                    }
                }
                secondTry = 0;
            }
            //Prints out all players points
            for (int i = 0; i < playerPoints.length; i++) {
                System.out.println("Player " + i + " has " + playerPoints[i] + " points." + "\n");
            }
            //Clears the players hands and re deals cards
            for (int i = 0; i < players.length; i++) {
                players[i].playersHand.hand1.clear();
                players[i].splitHand.hand1.clear();
            }
            dealHands(d);
        }
        System.out.println("HOW MANY TIMES - " + howLongs);
        System.out.println("HOW MANY SPLITS - " + splitValue);
        System.out.println("-----------------ROUND END-----------------");
    }

    //This sets all player points to 100 and plays a full game
    public static void playTestGame() {
        BasicPlayer[] p = new BasicPlayer[numberOfPlayers];
        int player1Points = 100;
        int player2Points = 100;
        int player3Points = 100;
        playerPoints = new int[]{100, 100, 100, 100};

        //Creates all players, gives first player Dealer Strategy
        for (Player p1 : p) {
            p[0] = new BasicPlayer(0, new Hand(), new Hand(), new DealerStrategy());
            p[1] = new BasicPlayer(1, new Hand(), new Hand(), new BasicStrategy());
            p[2] = new BasicPlayer(2, new Hand(), new Hand(), new BasicStrategy());
            p[3] = new BasicPlayer(3, new Hand(), new Hand(), new BasicStrategy());
        }
        //Set how many games are played
        for (int i = 0; i < 1; i++) {

            System.out.println("GAME START --------------------------------------------------------------------");
            BasicGame bg = new BasicGame(p);
            bg.playMatch();
            gameCount++;
            System.out.println(gameCount);
            System.out.println("GAME OVER --------------------------------------------------------------------" + "\n");
//            System.out.println(testing.size() + "\n");
//            System.out.println(testing);
        }
    }

    public static void main(String[] args) {
        playTestGame();
    }

}
