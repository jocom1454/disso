/*
Student Number - 100211671
Author - ukk17cbu
This is the Round class for the Blackjack simulation.
This class can find the winner for a select hand. The issue with this class
is that the game was built around it. This class' methods can be used but they're not
called in the BasicGame method. FindWinner is rebuilt in BasicGame.
 */
package Blackjack;

import Cards.*;
import Cards.Hand;

public class Round {
    public BasicPlayer[] players;
    public Hand floor;
    public Card leadCard;
    int leadPlayer;
    BasicPlayer dealer;
    public int upCard;

    public Round(int p) {
        leadPlayer = p;
        this.players = new BasicPlayer[4];
        this.upCard = 1;

    }

    //This method sets the dealer
    public void setDealer() {
        dealer = players[0];
    }

    //This method sets the upCard
    public void setUpCard(int upCard){
        this.upCard = upCard;
    }

    //This is an accessor method for the upCard
    public int getUpCard(){
        return this.upCard;
    }

    //This was the orignal check for split method, before it was moved into hand as that made more sense
    public boolean checkForSplit() {
        boolean decision = false;
        Card upCard = dealer.playersHand.hand1.get(0);
        for (int i = 1; i < players.length; i++) {
            Hand playersHand = players[i].playersHand;
            Card playersFirst = playersHand.hand1.get(0);
            Card.Rank playersRank = playersFirst.rank;
            if (playersHand.checkPlayerDouble()) {
                if (upCard.rank == Card.Rank.TWO) {
                    if (playersRank != Card.Rank.FOUR) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.THREE) {
                    if (playersRank != Card.Rank.FOUR) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.FOUR) {
                    if (playersRank != Card.Rank.FOUR) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.FIVE) {
                    decision = true;

                } else if (upCard.rank == Card.Rank.SIX) {
                    decision = true;

                } else if (upCard.rank == Card.Rank.SEVEN) {
                    if (playersRank == Card.Rank.SEVEN) {
                        decision = true;
                    } else if (playersRank == Card.Rank.THREE) {
                        decision = true;
                    } else if (playersRank == Card.Rank.TWO) {
                        decision = true;
                    }

                } else if (upCard.rank == Card.Rank.EIGHT) {
                    if (playersRank == Card.Rank.NINE) {
                        decision = true;
                    }
                } else if (upCard.rank == Card.Rank.NINE) {
                    if (playersRank == Card.Rank.NINE) {
                        decision = true;
                    }
                }
            }
        }
        return decision;
    }


    //This can find the winner after a hand has been completed. It is explained in BasicGame.
    public int findWinner() {
        int result = 0;
            System.out.println(players[0].playersHand);
            int dealer = (int) players[0].playersHand.handValues().get(1);
            System.out.println(dealer);
            for (int i = 0; i < players.length; i++) {
                if ((int) players[i].playersHand.handValues().get(1) > dealer) {
                    result = i;
                } else if ((int) players[i].playersHand.handValues().get(1) == dealer) {
                    result = 5;
                } else {
                    result = 0;
                }
            }

        return result;
    }


}
