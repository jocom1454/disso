/*
Student Number - 100211671
Author - ukk17cbu
This is the Player Interface class for the Blackjack simulation. This is a collection of cards.
 */
package Blackjack;

import Cards.*;
import Cards.Card.Suit;

import java.util.ArrayList;

/**
 * interface for a Player
 * @author ajb
 */
public interface Player {
    /**
     * Adds card c to this players hand
     * @param c
     */
    void dealCard(Card c);
    /**
     * Allows for external setting of player strategy
     * @param s
     */
    void setStrategy(Strategy s);
    /**
     * Determines which of the players cards to play based on the in play trick t
     * and player strategy
     * @return card to play
     */

    ArrayList choice(Round r);

    /**
     * Game passes the players the completed trick
     */
    void viewRound(Round r);
    int getID();
}
