100211761 - Blackjack Simulation

Description -
This simulation can play a full game of blackjack and give
players card counting methods. 

How to Run -
To run this game, go to BasicGame and run the class. 
The game is already set up to run

Changing Card Counting Methods -
To change the card counting methods the following needs to be done -
1) In BasicGame, find lines 216, 220, 224, 333, 336 and 340.
2) The game is currently set to the Omega Count
3) To change the card counting method replace "setOmegaCount" with one of the following -
    
    a)"setHiLoCount"
    
    b)"setWongCount"
    
    c)"setKISSCount"
    
    d)"setKOCount"
    
    e)"setZenCount"
    
Any of these will change the card counting method for all players.
All 6 of the lines mentioned will need to be changed for the game 
to work properly.

What to expect -
The game outlines who won and is very clear about all
decisions made by players and shows it all. All split hands
are shown and the results at the end of each hand is displayed.
